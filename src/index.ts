import Vue from "vue";

import router from "./components/router";
import App from "./components/App.vue";

let v = new Vue({
    el: "#app",
    router,
    template: `<App/>`,
    components: { App }
});