import axios from 'axios';

const APICall = axios.create({
    baseURL: 'http://localhost:5000',
    timeout: 10000,
    withCredentials: true,
    transformRequest: [(data) => JSON.stringify(data)],
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
});

export default APICall;