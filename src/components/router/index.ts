import Vue from 'vue'
import Router from 'vue-router'
import Home from '../entries/Home.vue'
import Status from '../entries/Status.vue'

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        { path: '/', component: Home },
        { path: '/status', component: Status },
    ]
})