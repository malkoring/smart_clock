from flask import Blueprint, jsonify
import RPi.GPIO as GPIO
import Adafruit_DHT

room_status = Blueprint('room_status', __name__)

# 상태를 지속적으로 읽어들이는 API Call
@room_status.route('/api/status')
def status_listen():
    
    sensor=Adafruit_DHT.DHT11
    gpio=17
    
    humidity, temperature = Adafruit_DHT.read_retry(sensor, gpio)
    
    print("Temperature" + str(temperature) + " C / Humidity " + str(humidity) + " %")
        
    return jsonify(temperature=temperature, humidity=humidity)
