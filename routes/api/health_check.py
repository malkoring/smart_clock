from flask import Blueprint, jsonify

health_check = Blueprint('status', __name__)

# 상태를 지속적으로 읽어들이는 API Call
@health_check.route('/api/health_check')
def check():
    with open('/etc/state.txt', 'r') as myfile:
            state=myfile.read().replace('\n', '')
    return jsonify(current_state=state)
    
