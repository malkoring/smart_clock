from flask import Flask, Blueprint

import RPi.GPIO as GPIO
import time
import threading

# initialize GPIO


GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)


app = Flask(__name__)


def listen_button_input():
    while(True):
        time.sleep(0.5)
        input_state = GPIO.input(18)
        if input_state == False:
            state = ''
            with open('/etc/state.txt', 'r') as myfile:
                state=myfile.read().replace('\n', '')

            print(state)

            with open('/etc/state.txt', 'w') as myfile:
                if state=='clock':
                    myfile.write('status')
                else:
                    myfile.write('clock')
                 


                    
from routes.api.health_check import health_check
from routes.api.room_status import room_status
from routes.main import main


app.register_blueprint(health_check)
app.register_blueprint(room_status)
app.register_blueprint(main)


if __name__ == '__main__':
    threading.Thread(target=listen_button_input).start()
    app.run(debug=True, threaded=True)
